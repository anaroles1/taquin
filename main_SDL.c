#include <stdio.h>
#include <stdlib.h>
#include "structures_SDL.h"
#include <time.h>
#include <math.h>
#include <SDL/SDL.h>
#include <SDL_keysym.h> 

int fichier_texte(char* s){
	/*renvoie 1 si s se termine par ".txt" et 0 sinon*/
	int i = 0;
	while(s[i] != '\0'){i++;}
	if(i>4 && s[i-4]=='.' && s[i-3]=='t' && s[i-2]=='x' && s[i-1]=='t'){return 1;}
	return 0;
}

void charger_fichier(char* nom_fichier, struct taquin * plt){

	FILE* fichier = fopen(nom_fichier, "r");
	if(fichier == NULL){
		perror("probleme a l\'ouverture du fichier");
		exit(-1);
	}

	double nombre_elements = 0; //double pour pouvoir utiliser sqrt
	int n = 1;
	int a;
	while(n == 1){
		n = fscanf(fichier, "%d", &a);
		nombre_elements += 1;
	}

	rewind(fichier);
	plt->taille = (int) sqrt(nombre_elements);
	plt->grille = malloc(plt->taille*sizeof(int*));
	if(plt->grille == NULL){
		perror("probleme d\'allocation memoire");
		exit(-3);
	}
	for(int i = 0 ; i < plt->taille ; i++){

		plt->grille[i] = malloc(plt->taille*sizeof(int));
		if(plt->grille[i] == NULL){
			perror("probleme d\'allocation memoire");
			exit(-3);
		}

		for(int j = 0 ; j < plt->taille ; j++){
			n = fscanf(fichier, "%d", &(plt->grille[i][j]));
			if(n != 1 || plt->grille[i][j] < 0 || plt->grille[i][j] >= (int) nombre_elements){
				perror("grille invalide");
				exit(-4);
			}
			if(plt->grille[i][j] == 0){
				plt->x = i;
				plt->y = j;
			}
		}
	}
	plt->init = 1;
}

int deplacement( struct taquin * plateau, char t){
	//déplace la case vide selon la touche t, renvoie 1 si le déplacement a bien été effectué

	if(t == 'h'){
		if(plateau->x > 0){
			(plateau->grille)[plateau->x][plateau->y] = (plateau->grille)[(plateau->x)-1][plateau->y];
			plateau->x -= 1;
			(plateau->grille)[plateau->x][plateau->y] = 0;
		}
		else{return 0;}
	}

	if(t == 'b'){
		if(plateau->x < plateau->taille-1){
			plateau->grille[plateau->x][plateau->y] = plateau->grille[plateau->x+1][plateau->y];
			plateau->x += 1;
			plateau->grille[plateau->x][plateau->y] = 0;
		}
		else{return 0;}
	}

	if(t == 'g'){
		if(plateau->y > 0){
			plateau->grille[plateau->x][plateau->y] = plateau->grille[plateau->x][plateau->y-1];
			plateau->y -= 1;
			plateau->grille[plateau->x][plateau->y] = 0;
		}
		else{return 0;}
	}

	if(t == 'd'){
		if(plateau->y < plateau->taille-1){
			plateau->grille[plateau->x][plateau->y] = plateau->grille[plateau->x][plateau->y+1];
			plateau->y += 1;
			plateau->grille[plateau->x][plateau->y] = 0;
		}
		else{return 0;}
	}

	return 1;
}

void verifier_grille(struct taquin * plt){
	for(int i = 0 ; i < plt->taille ; i++){
		for(int j = 0 ; j < plt->taille ; j++){
			if(plt->grille[i][j] != i*(plt->taille)+j+1 && plt->grille[i][j] != 0){return;}
		}
	}
	plt->complet = 1;
	return;
}

void boucleEvent(struct taquin plt){
	SDL_Event keys; 								//pour récupérer les évènements clavier
	int flag = 1;
	while((flag != 0) && (plt.complet == 0)){								
		SDL_WaitEvent(&keys);						//attente d'un évènement, programme bloqué pour éco les ressources
		switch(keys.type){
			case SDL_QUIT : flag = 0;				//fin de boucle si SDL quittée
			case SDL_KEYDOWN :						//cas d'une touche enfoncée
				switch(keys.key.keysym.sym){		//récupération de la touche
					case SDLK_UP :
						if(deplacement(&plt, 'h') != 1){
							printf("deplacement impossible");
						}
						break;
					case SDLK_DOWN :
						if(deplacement(&plt, 'b') != 1){
							printf("deplacement impossible");
						}
						break;
					case SDLK_RIGHT :
						if(deplacement(&plt, 'd') != 1){
							printf("deplacement impossible");
						}
						break;
					case SDLK_LEFT :
						if(deplacement(&plt, 'g') != 1){
							printf("deplacement impossible");
						}
						break;
					case 'q' : 										//appuer sur 'q' permet de quitter la boucle d'events
						flag = 0;
						break;
					default : 
						printf("deplacement inconnu");
						break;

				}
		}
		if(plt.x == plt.taille-1 && plt.y == plt.taille-1){
			verifier_grille(&plt);
		}
	}
	if(plt.complet != 0){
		printf("Gagné !");
	}
}

int main(int argc, char* argv[]){

	/*le programme prend un argument qui est soit un entier
	correspondant à la taille de la grille soit un fichier
	texte contenant une grille*/

	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) != 0){
		fprintf(stderr,"\nUnable to initializeSDL:  %s\n", SDL_GetError()  );
		exit(EXIT_FAILURE) ;
	}

	struct taquin plateau;											//plateau de jeu
	plateau.init  = 0;
	plateau.complet = 0;
	plateau.taille = 4;												//taille par défaut
	if(argc > 1){													//si le programme possède un argument
		char* arg = argv[1];
		if(fichier_texte(arg)){										//si l'argument est un nom de fichier, on charge la grille
			charger_fichier(arg, &plateau);
		}

		else{
			if(sscanf(arg, "%d", &plateau.taille) != 1){			//sinon on modifie la taille de la grille
				perror("erreur argument\n");
				exit(-2);
			}
			if(plateau.taille < 2 || plateau.taille > 10){			//on limite la taille à 10 pour pouvoir afficher la grille correctement
				perror("taille incorrecte");
				exit(-2);
			}
		}
	}

	if(plateau.init == 0){											//si la grille n'a pas été initialisée, i.e on a pas chargé de fichier
		plateau.complet = 0;
		plateau.grille = malloc(plateau.taille*sizeof(int*));
		if(plateau.grille == NULL){
			perror("probleme d\'allocation memoire");
			exit(-3);
		}
		for(int i = 0 ; i<plateau.taille ; i++){

			plateau.grille[i] = malloc(plateau.taille*sizeof(int));
			if(plateau.grille[i] == NULL){
				perror("probleme d\'allocation memoire");
				exit(-3);
			}

			for(int j = 0 ; j<plateau.taille ; j++){
				plateau.grille[i][j] = plateau.taille*i+j+1;
			}
		}
		plateau.x = plateau.taille-1;
		plateau.y = plateau.taille-1;
		plateau.grille[plateau.x][plateau.y] = 0;					//on représente la case vide par la valeur 0
		//à ce stade la grille est dans la configuration Tid.
		//on va maintenant mélanger la grille
		srand(time(NULL));
		int nbr_deplacements;
		int carre_taille = plateau.taille*plateau.taille;
		do{nbr_deplacements = rand()%(2*carre_taille+1);}
		while(nbr_deplacements < carre_taille/2);					//on décide arbitrairement d'avoir entre n²/2 et 2n² mouvements dans le mélange
		int deplacements_effectues = 0;
		int dir;
		char directions[4] = {'h','b','g','d'};
		while(deplacements_effectues < nbr_deplacements){
			dir = rand()%4;
			deplacements_effectues += deplacement(&plateau, directions[dir]);
		}
		plateau.init = 1;
	}

	/**************************Affichage************************/

	SDL_Surface * taquin = NULL ;
	if((taquin = SDL_SetVideoMode(100*plateau.taille,100*plateau.taille,32, SDL_HWSURFACE)) == NULL){
		fprintf(stderr,"Erreur VideoMode  %s\n",SDL_GetError());
		exit(EXIT_FAILURE) ;
	}

	int carre_taille = plateau.taille*plateau.taille;
	SDL_Surface** cases = malloc(carre_taille*sizeof(SDL_Surface*));
	if(cases == NULL){
		perror("probleme d\'allocation memoire");
		exit(-3);
	}
	for(int i = 0 ; i<carre_taille ; i++){
		cases[i] = NULL;
	}

	FILE* images = fopen("images/liste_images.txt", "r");
	if(images == NULL){
		perror("probleme a l\'ouverture du fichier");
		exit(-1);
	}

	char* nom_fichier = malloc(8*sizeof(char));
	if(nom_fichier == NULL){
		perror("probleme d\'allocation memoire");
		exit(-3);
	}

	for(int i = 0 ; i<carre_taille ; i++){
		fscanf(images, "%s", nom_fichier);
		if((cases[i] = SDL_LoadBMP(nom_fichier)) == NULL){
			fprintf(stderr,"Erreur VideoMode  %s\n",SDL_GetError());
			exit(EXIT_FAILURE) ;
		}
	}
	free(nom_fichier);
	fclose(images);

	SDL_Rect position;
	for(int i = 0 ; i<plateau.taille ; i++){
		for(int j = 0 ; j<plateau.taille ; j++){
			position.x = 100*j;
			position.y = 100*i;
			SDL_BlitSurface(cases[plateau.grille[i][j]],NULL,taquin,&position);
		}
	}

	SDL_Flip(taquin);

	/************************************************************/

	char touche[2];
	while(plateau.complet == 0){

		//on utilise les touches gdhb
		printf("deplacement : ");
		scanf("%s", touche);
		printf("\n");

		if(touche[0] != 'g' && touche[0] != 'd' && touche[0] != 'h' && touche[0] != 'b'){
			printf("deplacement inconnu\n");
		}

		else{
			if(deplacement(&plateau, touche[0]) != 1){printf("deplacement impossible\n");}
		};

		for(int i = 0 ; i<plateau.taille ; i++){
			for(int j = 0 ; j<plateau.taille ; j++){
				position.x = 100*j;
				position.y = 100*i;
				SDL_BlitSurface(cases[plateau.grille[i][j]],NULL,taquin,&position);
			}
		}

		SDL_Flip(taquin);

		if(plateau.x == plateau.taille-1 && plateau.y == plateau.taille-1){
			verifier_grille(&plateau);
		}
	}

	printf("Gagne !\n");

	for(int i = 0 ; i<plateau.taille ; i++){
		free(plateau.grille[i]);
	}
	free(plateau.grille);

	SDL_Quit();

	return 0;
}