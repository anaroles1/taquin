#include <stdio.h>
#include <stdlib.h>
#include "structures.h"
#include <time.h>
#include <math.h>

int fichier_texte(char* s){
	/*renvoie 1 si s se termine par ".txt" et 0 sinon*/
	int i = 0;
	while(s[i] != '\0'){i++;}
	if(i>4 && s[i-4]=='.' && s[i-3]=='t' && s[i-2]=='x' && s[i-1]=='t'){return 1;}
	return 0;
}

void charger_fichier(char* nom_fichier, struct taquin * plt){

	FILE* fichier = fopen(nom_fichier, "r");
	if(fichier == NULL){
		perror("probleme a l\'ouverture du fichier");
		exit(-1);
	}

	double nombre_elements = 0; //double pour pouvoir utiliser sqrt
	int n = 1;
	int a;
	while(n == 1){
		n = fscanf(fichier, "%d", &a);
		nombre_elements += 1;
	}

	rewind(fichier);
	plt->taille = (int) sqrt(nombre_elements);
	plt->grille = malloc(plt->taille*sizeof(int*));
	if(plt->grille == NULL){
		perror("probleme d\'allocation memoire");
		exit(-3);
	}
	for(int i = 0 ; i < plt->taille ; i++){

		plt->grille[i] = malloc(plt->taille*sizeof(int));
		if(plt->grille[i] == NULL){
			perror("probleme d\'allocation memoire");
			exit(-3);
		}

		for(int j = 0 ; j < plt->taille ; j++){
			n = fscanf(fichier, "%d", &(plt->grille[i][j]));
			if(n != 1 || plt->grille[i][j] < 0 || plt->grille[i][j] >= (int) nombre_elements){
				perror("grille invalide");
				exit(-4);
			}
			if(plt->grille[i][j] == 0){
				plt->x = i;
				plt->y = j;
			}
		}
	}
	plt->init = 1;
}

int deplacement( struct taquin * plateau, char t){
	//déplace la case vide selon la touche t, renvoie 1 si le déplacement a bien été effectué

	if(t == 'h'){
		if(plateau->x > 0){
			(plateau->grille)[plateau->x][plateau->y] = (plateau->grille)[(plateau->x)-1][plateau->y];
			plateau->x -= 1;
			(plateau->grille)[plateau->x][plateau->y] = 0;
		}
		else{return 0;}
	}

	if(t == 'b'){
		if(plateau->x < plateau->taille-1){
			plateau->grille[plateau->x][plateau->y] = plateau->grille[plateau->x+1][plateau->y];
			plateau->x += 1;
			plateau->grille[plateau->x][plateau->y] = 0;
		}
		else{return 0;}
	}

	if(t == 'g'){
		if(plateau->y > 0){
			plateau->grille[plateau->x][plateau->y] = plateau->grille[plateau->x][plateau->y-1];
			plateau->y -= 1;
			plateau->grille[plateau->x][plateau->y] = 0;
		}
		else{return 0;}
	}

	if(t == 'd'){
		if(plateau->y < plateau->taille-1){
			plateau->grille[plateau->x][plateau->y] = plateau->grille[plateau->x][plateau->y+1];
			plateau->y += 1;
			plateau->grille[plateau->x][plateau->y] = 0;
		}
		else{return 0;}
	}

	return 1;
}

void afficher_grille(struct taquin plt){
	for(int i = 0 ; i < plt.taille ; i++){
		for(int j = 0 ; j < plt.taille ; j++){
			printf("%3d ", plt.grille[i][j]);
		}
		printf("\n");
	}
}

void verifier_grille(struct taquin * plt){
	for(int i = 0 ; i < plt->taille ; i++){
		for(int j = 0 ; j < plt->taille ; j++){
			if(plt->grille[i][j] != i*(plt->taille)+j+1 && plt->grille[i][j] != 0){return;}
		}
	}
	plt->complet = 1;
	return;
}

int main(int argc, char* argv[]){

	/*le programme prend un argument qui est soit un entier
	correspondant à la taille de la grille soit un fichier
	texte contenant une grille*/

	struct taquin plateau;											//plateau de jeu
	plateau.init  = 0;
	plateau.complet = 0;
	plateau.taille = 4;												//taille par défaut
	if(argc > 1){													//si le programme possède un argument
		char* arg = argv[1];
		if(fichier_texte(arg)){										//si l'argument est un nom de fichier, on charge la grille
			charger_fichier(arg, &plateau);
		}

		else{
			if(sscanf(arg, "%d", &plateau.taille) != 1){			//sinon on modifie la taille de la grille
				perror("taille incorrecte\n");
				exit(-2);
			}
		}
	}

	if(plateau.init == 0){											//si la grille n'a pas été initialisée, i.e on a pas chargé de fichier
		plateau.complet = 0;
		plateau.grille = malloc(plateau.taille*sizeof(int*));
		if(plateau.grille == NULL){
			perror("probleme d\'allocation memoire");
			exit(-3);
		}
		for(int i = 0 ; i<plateau.taille ; i++){

			plateau.grille[i] = malloc(plateau.taille*sizeof(int));
			if(plateau.grille[i] == NULL){
				perror("probleme d\'allocation memoire");
				exit(-3);
			}

			for(int j = 0 ; j<plateau.taille ; j++){
				plateau.grille[i][j] = plateau.taille*i+j+1;
			}
		}
		plateau.x = plateau.taille-1;
		plateau.y = plateau.taille-1;
		plateau.grille[plateau.x][plateau.y] = 0;					//on représente la case vide par la valeur 0
		//à ce stade la grille est dans la configuration Tid.
		//on va maintenant mélanger la grille
		srand(time(NULL));
		int nbr_deplacements;
		int carre_taille = plateau.taille*plateau.taille;
		do{nbr_deplacements = rand()%(2*carre_taille+1);}
		while(nbr_deplacements < carre_taille/2);					//on décide arbitrairement d'avoir entre n²/2 et 2n² mouvements dans le mélange
		int deplacements_effectues = 0;
		int dir;
		char directions[4] = {'h','b','g','d'};
		while(deplacements_effectues < nbr_deplacements){
			dir = rand()%4;
			deplacements_effectues += deplacement(&plateau, directions[dir]);
		}
		plateau.init = 1;
	}

	afficher_grille(plateau);
	char touche[2];
	while(plateau.complet == 0){

		//on utilise les touches gdhb
		printf("deplacement : ");
		scanf("%s", touche);
		printf("\n");

		if(touche[0] != 'g' && touche[0] != 'd' && touche[0] != 'h' && touche[0] != 'b'){
			printf("deplacement inconnu\n");
		}

		else{
			if(deplacement(&plateau, touche[0]) != 1){printf("deplacement impossible\n");}
		};

		afficher_grille(plateau);

		if(plateau.x == plateau.taille-1 && plateau.y == plateau.taille-1){
			verifier_grille(&plateau);
		}
	}

	printf("Gagne !\n");

	for(int i = 0 ; i<plateau.taille ; i++){
		free(plateau.grille[i]);
	}
	free(plateau.grille);

	return 0;
}