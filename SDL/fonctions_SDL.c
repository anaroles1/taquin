#include "fonctions_SDL.h"

void boucleEvent(struct taquin * plt, SDL_Surface * taquin, SDL_Surface ** cases, pile * solve_pile){
    SDL_Event event;                                 //pour récupérer les évènements clavier
    int flag = 1;                                     //touche pour résolution
    while((flag != 0) && (plt->complet == 0)){                      
        SDL_WaitEvent(&event);                    	//attente d'un évènement, programme bloqué pour éco les ressources
        if(event.type == SDL_QUIT){
            flag = 0;
        }
        else{
            if(event.type == SDL_KEYDOWN){
                if((event.key.keysym.sym == 'h')&&(plt->solution == 1)){     //si la solution existe
                    pile * pile_aux = malloc(sizeof(pile*));
                    if(pile_aux == NULL){
                        perror("probleme d\'allocation memoire");
                        exit(-3);
                    }
                    pile_aux->premier = NULL;

                    SDLKey k;
                    do{
                        k = depiler(solve_pile);

                        int cont=1 ;
                        SDL_Event event_bis ;
                        while(cont){

                            SDL_WaitEvent(&event_bis) ;
                            switch(event_bis.type){

                                case SDL_KEYDOWN:
                                    cont = 0;
                                    if(event_bis.key.keysym.sym == SDLK_ESCAPE){   //arrêt d'affichage de la résolution
                                        empiler(solve_pile, k);
                                        k = 0;
                                    }
                                    break ;

                                default :
                                    cont= 1 ;
                            }
                        }

                        switch(k){
                            case SDLK_RIGHT:
                                deplacement(plt, SDLK_LEFT, taquin, cases, pile_aux);       //mouvements inverse
                                break;                                                      //pour afficher la solution
                            case SDLK_LEFT:
                                deplacement(plt, SDLK_RIGHT, taquin, cases, pile_aux);
                                break;
                            case SDLK_UP:
                                deplacement(plt, SDLK_DOWN, taquin, cases, pile_aux);
                                break;
                            case SDLK_DOWN:
                                deplacement(plt, SDLK_UP, taquin, cases, pile_aux);
                                break;
                        }
                    }
                    while(k != 0);

                    while(depiler(pile_aux) != 0){}                //permet de free les elements de la pile
                    free(pile_aux);
                }
                else{
                    deplacement(plt, event.key.keysym.sym, taquin, cases, solve_pile);
                }
            }
            else{
                //gestion de la souris
                if((event.type == SDL_MOUSEBUTTONDOWN)&&(event.button.button == SDL_BUTTON_LEFT)){
                    int x_mouse = (int)(event.button.x/100);
                    int y_mouse = (int)(event.button.y/100);
                    if((x_mouse == plt->y+1) && (y_mouse == plt->x)){
                        deplacement(plt, SDLK_RIGHT,taquin, cases, solve_pile);
                    }
                    else{
                        if((x_mouse == plt->y-1) && (y_mouse == plt->x)){
                            deplacement(plt, SDLK_LEFT, taquin, cases, solve_pile);
                        }
                        else{
                            if((y_mouse == plt->x+1) && (x_mouse == plt->y)){
                                deplacement(plt, SDLK_DOWN, taquin, cases, solve_pile);
                            }
                            else{
                                if((y_mouse == plt->x-1) && (x_mouse == plt->y)){
                                    deplacement(plt, SDLK_UP, taquin, cases, solve_pile);
                                }
                            }
                        }
                    }
                }
            }
        }
        if(plt->x == plt->taille-1 && plt->y == plt->taille-1){
            verifier_grille(plt);
        }
    }
    if(plt->complet == 1){

        SDL_FreeSurface(taquin);
        if((taquin = SDL_SetVideoMode(400,400,32, SDL_HWSURFACE)) == NULL){
			fprintf(stderr,"Erreur VideoMode  %s\n",SDL_GetError());
			exit(EXIT_FAILURE) ;
		}

        SDL_Surface * ecran_fin;
        if((ecran_fin = SDL_LoadBMP("images/gagne.bmp")) == NULL){
			fprintf(stderr,"Erreur VideoMode  %s\n",SDL_GetError());
			exit(EXIT_FAILURE) ;
		}

		SDL_Rect position;
		position.x = 0;
		position.y = 0;
		SDL_BlitSurface(ecran_fin,NULL,taquin,&position);

		SDL_Flip(taquin);
		pause();
    }
}

void pause(){
    int cont=1 ;
    SDL_Event event ;
    while(cont){

        SDL_WaitEvent(&event) ;
        switch(event.type){

            case SDL_QUIT:
                cont = 0;
                break ;

            default :
                cont= 1 ;
        }
    }
}
