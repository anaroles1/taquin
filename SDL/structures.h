#include <SDL/SDL.h>

#ifndef structures
#define structures

struct taquin{
	int** grille;		//matrice n*n représentatn la grille
	int taille;			//taille de la grille
	int x;
	int y;				//x et y sont les corrdonnées de la case vide
	int init; 			//booléen permettant de savoir si la grille a été initialisée (0 non, 1 oui)
	int complet;		//booléen permettant de savoir si la partie est gagnée
	int solution;		//bolléen permettant de savoir si on connait une solution
};

typedef struct element element;
struct element{
    SDLKey touche;
    element* suivant;
};

typedef struct pile pile;
struct pile{
    element* premier;
};

#endif