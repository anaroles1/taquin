#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <SDL/SDL.h>
#include <SDL_keysym.h>
#include "structures.h"
#include "fonctions.h"
#include "fonctions_SDL.h"
#include "pile.h"

int main(int argc, char* argv[]){

	/*le programme prend un argument qui est soit un entier
	correspondant à la taille de la grille soit un fichier
	texte contenant une grille*/

	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) != 0){
		fprintf(stderr,"\nUnable to initializeSDL:  %s\n", SDL_GetError()  );
		exit(EXIT_FAILURE) ;
	}

	struct taquin plateau;											//plateau de jeu
	plateau.init  = 0;
	plateau.complet = 0;
	plateau.taille = 4;												//taille par défaut
	pile * p = malloc(sizeof(pile*));
	if(p == NULL){
		perror("probleme d\'allocation memoire");
		exit(-3);
	}
	p->premier = NULL;

	if(argc > 1){													//si le programme possède un argument
		char* arg = argv[1];
		if(fichier_texte(arg)){										//si l'argument est un nom de fichier, on charge la grille
			charger_fichier(arg, &plateau);
			plateau.solution = 0;
		}

		else{
			if(sscanf(arg, "%d", &plateau.taille) != 1){			//sinon on modifie la taille de la grille
				perror("erreur argument\n");
				exit(-2);
			}
			if(plateau.taille < 2 || plateau.taille > 10){			//on limite la taille à 10 pour pouvoir afficher la grille correctement
				perror("taille incorrecte");
				exit(-2);
			}
		}
	}

	if(plateau.init == 0){											//si la grille n'a pas été initialisée, i.e on a pas chargé de fichier
		plateau.grille = malloc(plateau.taille*sizeof(int*));
		if(plateau.grille == NULL){
			perror("probleme d\'allocation memoire");
			exit(-3);
		}
		for(int i = 0 ; i<plateau.taille ; i++){

			plateau.grille[i] = malloc(plateau.taille*sizeof(int));
			if(plateau.grille[i] == NULL){
				perror("probleme d\'allocation memoire");
				exit(-3);
			}

			for(int j = 0 ; j<plateau.taille ; j++){
				plateau.grille[i][j] = plateau.taille*i+j+1;
			}
		}
		plateau.x = plateau.taille-1;
		plateau.y = plateau.taille-1;
		plateau.grille[plateau.x][plateau.y] = 0;					//on représente la case vide par la valeur 0
		//à ce stade la grille est dans la configuration Tid.
		//on va maintenant mélanger la grille
		srand(time(NULL));
		int nbr_deplacements;
		int carre_taille = plateau.taille*plateau.taille;
		do{nbr_deplacements = rand()%(4*carre_taille+1);}
		while(nbr_deplacements < carre_taille);						//on décide arbitrairement d'avoir entre n² et 4n² mouvements dans le mélange
		int deplacements_effectues = 0;
		int dir;
		SDLKey directions[4] = {SDLK_UP,SDLK_DOWN,SDLK_LEFT,SDLK_RIGHT};
		while(deplacements_effectues < nbr_deplacements){
			dir = rand()%4;
			deplacements_effectues += deplacement(&plateau, directions[dir], NULL, NULL, p);
		}
		plateau.init = 1;
		plateau.solution = 1;
	}

	/**************************Affichage************************/

	SDL_Surface * taquin = NULL ;
	if((taquin = SDL_SetVideoMode(100*plateau.taille,100*plateau.taille,32, SDL_HWSURFACE)) == NULL){
		fprintf(stderr,"Erreur VideoMode  %s\n",SDL_GetError());
		exit(EXIT_FAILURE) ;
	}

	int carre_taille = plateau.taille*plateau.taille;
	SDL_Surface** cases = malloc(carre_taille*sizeof(SDL_Surface*));
	if(cases == NULL){
		perror("probleme d\'allocation memoire");
		exit(-3);
	}
	for(int i = 0 ; i<carre_taille ; i++){
		cases[i] = NULL;
	}

	FILE* images = fopen("images/liste_images.txt", "r");
	if(images == NULL){
		perror("probleme a l\'ouverture du fichier");
		exit(-1);
	}

	char* nom_fichier = malloc(14*sizeof(char));
	if(nom_fichier == NULL){
		perror("probleme d\'allocation memoire");
		exit(-3);
	}

	for(int i = 0 ; i<carre_taille ; i++){
		fscanf(images, "%s", nom_fichier);
		if((cases[i] = SDL_LoadBMP(nom_fichier)) == NULL){
			fprintf(stderr,"Erreur VideoMode  %s\n",SDL_GetError());
			exit(EXIT_FAILURE) ;
		}
	}
	free(nom_fichier);
	fclose(images);

	SDL_Rect position;
	for(int i = 0 ; i<plateau.taille ; i++){
		for(int j = 0 ; j<plateau.taille ; j++){
			position.x = 100*j;
			position.y = 100*i;
			SDL_BlitSurface(cases[plateau.grille[i][j]],NULL,taquin,&position);
		}
	}

	SDL_Flip(taquin);

	boucleEvent(&plateau, taquin, cases, p);

	for(int i = 0 ; i<plateau.taille ; i++){
		free(plateau.grille[i]);
	}

	while(depiler(p) != 0){}				//permet de free les elements de la pile
	free(p);
	free(plateau.grille);

	SDL_Quit();

	return 0;
}