#include <math.h>
#include "fonctions.h"

int fichier_texte(char* s){
	/*renvoie 1 si s se termine par ".txt" et 0 sinon*/
	int i = 0;
	while(s[i] != '\0'){i++;}
	if(i>4 && s[i-4]=='.' && s[i-3]=='t' && s[i-2]=='x' && s[i-1]=='t'){return 1;}
	return 0;
}

void charger_fichier(char* nom_fichier, struct taquin * plt){

	FILE* fichier = fopen(nom_fichier, "r");
	if(fichier == NULL){
		perror("probleme a l\'ouverture du fichier");
		exit(-1);
	}

	double nombre_elements = 0; //double pour pouvoir utiliser sqrt
	int n = 1;
	int a;
	while(n == 1){
		n = fscanf(fichier, "%d", &a);
		nombre_elements += 1;
	}

	rewind(fichier);
	plt->taille = (int) sqrt(nombre_elements);
	plt->grille = malloc(plt->taille*sizeof(int*));
	if(plt->grille == NULL){
		perror("probleme d\'allocation memoire");
		exit(-3);
	}
	for(int i = 0 ; i < plt->taille ; i++){

		plt->grille[i] = malloc(plt->taille*sizeof(int));
		if(plt->grille[i] == NULL){
			perror("probleme d\'allocation memoire");
			exit(-3);
		}

		for(int j = 0 ; j < plt->taille ; j++){
			n = fscanf(fichier, "%d", &(plt->grille[i][j]));
			if(n != 1 || plt->grille[i][j] < 0 || plt->grille[i][j] >= (int) nombre_elements){
				perror("grille invalide");
				exit(-4);
			}
			if(plt->grille[i][j] == 0){
				plt->x = i;
				plt->y = j;
			}
		}
	}
	plt->init = 1;
}

int deplacement(struct taquin * plateau, SDLKey k, SDL_Surface * taquin, SDL_Surface ** cases, pile * p){
	//déplace la case vide selon la touche t, renvoie 1 si le déplacement a bien été effectué

	int x1 = plateau->x, x2 = plateau->x, y1 = plateau->y, y2 = plateau->y; 		//on échange x1 et x2 et y1 et y2
	if(k == SDLK_UP){
		if(plateau->x > 0){
			(plateau->grille)[plateau->x][plateau->y] = (plateau->grille)[(plateau->x)-1][plateau->y];
			plateau->x -= 1;
			x2 -= 1;
			(plateau->grille)[plateau->x][plateau->y] = 0;
		}
		else{return 0;}
	}

	if(k == SDLK_DOWN){
		if(plateau->x < plateau->taille-1){
			plateau->grille[plateau->x][plateau->y] = plateau->grille[plateau->x+1][plateau->y];
			plateau->x += 1;
			x2 += 1;
			plateau->grille[plateau->x][plateau->y] = 0;
		}
		else{return 0;}
	}

	if(k == SDLK_LEFT){
		if(plateau->y > 0){
			plateau->grille[plateau->x][plateau->y] = plateau->grille[plateau->x][plateau->y-1];
			plateau->y -= 1;
			y2 -= 1;
			plateau->grille[plateau->x][plateau->y] = 0;
		}
		else{return 0;}
	}

	if(k == SDLK_RIGHT){
		if(plateau->y < plateau->taille-1){
			plateau->grille[plateau->x][plateau->y] = plateau->grille[plateau->x][plateau->y+1];
			plateau->y += 1;
			y2 += 1;
			plateau->grille[plateau->x][plateau->y] = 0;
		}
		else{return 0;}
	}

	if(taquin != NULL){
			SDL_Rect position;
			position.x = 100*y1;
			position.y = 100*x1;
			SDL_BlitSurface(cases[plateau->grille[x1][y1]],NULL,taquin,&position);
			position.x = 100*y2;
			position.y = 100*x2;
			SDL_BlitSurface(cases[plateau->grille[x2][y2]],NULL,taquin,&position);
			SDL_Flip(taquin);
	}

	empiler(p, k);
	return 1;
}

void verifier_grille(struct taquin * plt){
	for(int i = 0 ; i < plt->taille ; i++){
		for(int j = 0 ; j < plt->taille ; j++){
			if(plt->grille[i][j] != i*(plt->taille)+j+1 && plt->grille[i][j] != 0){return;}
		}
	}
	plt->complet = 1;
	return;
}