#include "pile.h"

void empiler(pile* p, SDLKey k){
    element* nouvel = malloc(sizeof(element*));
    if (p == NULL || nouvel == NULL){
        exit(EXIT_FAILURE);
    }

    nouvel->touche = k;
    nouvel->suivant = p->premier;
    p->premier = nouvel;
}

SDLKey depiler(pile* p){
    if (p == NULL){
        exit(EXIT_FAILURE);
    }

    SDLKey touche_depilee = 0;
    element* element_depile = p->premier;

    if (p != NULL && p->premier != NULL){
        touche_depilee = element_depile->touche;
        p->premier = element_depile->suivant;
        free(element_depile);
    }

    return touche_depilee;
}