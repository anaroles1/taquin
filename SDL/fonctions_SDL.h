#include <SDL/SDL.h>
#include "structures.h"
#include "fonctions.h"
#include "pile.h"

#ifndef fonctions_SDL
#define fonctions_SDL

void boucleEvent(struct taquin * plt, SDL_Surface * taquin, SDL_Surface ** cases, pile * solve_pile);
void pause();

#endif