#include <SDL/SDL.h>
#include "structures.h"

#ifndef fonctions
#define fonctions

int fichier_texte(char* s);
void charger_fichier(char* nom_fichier, struct taquin * plt);
int deplacement(struct taquin * plateau, SDLKey k, SDL_Surface * taquin, SDL_Surface ** cases, pile * p);
void verifier_grille(struct taquin * plt);

#endif